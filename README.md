## POL-ERASER ##

### 본 프로젝트는 free open-source project Eraser를 사용했습니다. ###

* POL-ERASER is **Free software** and its source code is released under GNU General Public License version 3.0 (GPLv3).

* 경찰청 사이버안전국 디지털포렌식센터 포렌식연구개발팀 FRND@police.go.kr

This application use Open Source Software (OSS). You can find the source code of these open source projects, along with applicable license information, below. We are deeply grateful to these developers for their work and contributions.
### Original Project is Eraser (GPLv3) ###
* Original Project - copyright 2008-2017 The Eraser Project

* Original Eraser Project- https://sourceforge.net/projects/eraser